## SET UP
1. docker-compose build
2. docker-compose up -d
3. copy file .env.example into .env
4. docker-compose exec app composer install
5. docker-compose exec nodejs npm install
6. docker-compose exec app php artisan migrate
7. docker-compose exec app php artisan db:seed

## CONFIG
- Open file host and add "127.0.0.1 merise.local" into it.
- Access:
   + website: https://adminlte.local/
   + DB: http://adminlte.local:8008/db_structure.php?server=1&db=adminlte
   + Mail: http://adminlte.local:8025/

