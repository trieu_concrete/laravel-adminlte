<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
    use Notifiable, HasRoles;
    use SoftDeletes;
    
    protected $guard_name   = 'web';

    protected $fillable = [
        'name', 
        'last_name', 
        'first_name', 
        'last_name_e', 
        'first_name_e', 
        'email', 
        'password', 
        'birthday', 
        'avatar', 
        'address', 
        'gender'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
