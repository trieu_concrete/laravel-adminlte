<?php

namespace App\Http\Requests\Admin\Administrator;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:255',
            'email'         => 'required|max:255|email|unique:admins',
            'password'      => 'required|max:20|min:8|confirmed',
            'last_name'     => 'required',
            'first_name'    => 'required',
            'last_name_e'   => 'required',
            'first_name_e'  => 'required',
            'birthday'      => 'required'
        ];
    }
}
