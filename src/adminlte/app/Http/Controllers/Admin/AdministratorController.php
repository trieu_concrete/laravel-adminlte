<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Administrator\StoreRequest;
use App\Http\Requests\Admin\Administrator\UpdateRequest;
use App\Models\Admin;

class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $administrators = Admin::role('Administrator');
        if ($request->name) {
            $administrators = $administrators->where('name', 'like', '%'.$request->name.'%');
        }
        if ($request->email) {
            $administrators = $administrators->where('email', 'like', '%'.$request->email.'%');
        }
        $administrators = $administrators->paginate(config('adminlte.admin.per_page'));
        return view('admin.administrator.index', compact('administrators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.administrator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        \DB::beginTransaction();
        try {
            $filename = null;
            if($request->hasFile('avatar')) {
                $file       = $request->file('avatar'); 
                $extension  = $file->getClientOriginalExtension();
                $filename   = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
                $uploadDir = 'administrator/';
                $file->storeAs($uploadDir, $filename, config('filesystems.public_disk'));
            }
            $data = [
                'name'          => $request->name,
                'last_name'     => $request->last_name,
                'first_name'    => $request->first_name,
                'last_name_e'   => $request->last_name_e,
                'first_name_e'  => $request->first_name_e,
                'email'         => $request->email,
                'password'      => bcrypt($request->password),
                'gender'        => $request->gender,
                'birthday'      => \Carbon\Carbon::parse($request->birthday)->format('Y-m-d'),
                'avatar'        => $filename,
                'address'       => $request->address,
            ];
            $admin =  Admin::create($data);
            $admin->assignRole('Administrator');

            \DB::commit();
            flash('Created Successfully!')->success()->important();
            return redirect()->route('administrators.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
            flash('Create Error')->error()->important();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = Admin::role('Administrator')->find($id);
        if ($admin !== null) {
            return view('admin.administrator.detail', compact('admin'));
        } else {
            flash('The Administrator could not be found!')->warning()->important();
            return redirect()->route('administrators.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::role('Administrator')->find($id);
        if ($admin !== null) {
            return view('admin.administrator.edit', compact('admin'));
        } else {
            flash('The Administrator could not be found!')->warning()->important();
            return redirect()->route('administrators.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        \DB::beginTransaction();
        try {
            $data_admin = [
                'name'          => $request->name,
                'last_name'     => $request->last_name,
                'first_name'    => $request->first_name,
                'last_name_e'   => $request->last_name_e,
                'first_name_e'  => $request->first_name_e,
                'email'         => $request->email,
                'gender'        => $request->gender,
                'birthday'      => \Carbon\Carbon::parse($request->birthday)->format('Y-m-d'),
                'address'       => $request->address,
            ];
            if ($request->password !== null) {
                $data_admin['password'] = bcrypt($request->password);
            }
            if($request->hasFile('avatar')) {
                $file       = $request->file('avatar'); 
                $extension  = $file->getClientOriginalExtension();
                $filename   = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
                $uploadDir = 'administrator/';
                $file->storeAs($uploadDir, $filename, config('filesystems.public_disk'));
                $data_admin['avatar'] = $filename;
                //delete image
                $avatar = Admin::find($id)->avatar;
                \Storage::disk(config('filesystems.public_disk'))->delete($uploadDir.$avatar);
            }
            Admin::role('Administrator')->where('id', $id)->update($data_admin);
            
            \DB::commit();
            flash('Update Successfully!')->success()->important();
            return redirect()->route('administrators.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
            flash('Update Error')->error()->important();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $admin = Admin::role('Administrator')->find($id);
            $admin->delete();
            $admin->removeRole('Administrator');
            \DB::commit();
            return back();
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::notice($e->getMessage());
            return back();
        }
    }
}
