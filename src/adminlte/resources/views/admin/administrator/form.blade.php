@php
    $isEdit = isset($admin->id) && $admin->id > 0 ? 1 : 0
@endphp
{!! Form::open(['method' => 'POST', 'url' => $isEdit ? route("administrators.update", $admin->id) : route("administrators.store"), 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
    @if($isEdit)
    @method('PUT')
    @endif
    <div class="box-header with-border">
        <h3 class="box-title">{{ $isEdit ? __('admin.texts.title_edit_administrator') : __('admin.texts.title_create_administrator') }}</h3>
    </div>
    @include('flash::message')
    <div class="box-body">
        @if($isEdit)
        <input type="hidden" value="{{$admin->id}}" name="id">
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'Name:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::text('name', $isEdit ? $admin->name : null, ['class' => 'form-control input-radius name '.($errors->has('name') ? 'is-invalid' : '')]) !!}
                        @if ($errors->has('name'))
                        <span class="text-danger invalid-feedback">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'Last Name:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::text('last_name', $isEdit ? $admin->last_name : null, ['class' => 'form-control input-radius last_name '.($errors->has('last_name') ? 'is-invalid' : '')]) !!}
                        @if ($errors->has('last_name'))
                        <span class="text-danger invalid-feedback">{{ $errors->first('last_name') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'First Name:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::text('first_name', $isEdit ? $admin->first_name : null, ['class' => 'form-control input-radius first_name '.($errors->has('first_name') ? 'is-invalid' : '')]) !!}
                        @if ($errors->has('first_name'))
                        <span class="text-danger invalid-feedback">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'English Last Name:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::text('last_name_e', $isEdit ? $admin->last_name_e : null, ['class' => 'form-control input-radius last_name_e '.($errors->has('last_name_e') ? 'is-invalid' : '')]) !!}
                        @if ($errors->has('last_name_e'))
                        <span class="text-danger invalid-feedback">{{ $errors->first('last_name_e') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'English First Name:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::text('first_name_e', $isEdit ? $admin->first_name_e : null, ['class' => 'form-control input-radius first_name_e '.($errors->has('last_name_e') ? 'is-invalid' : '')]) !!}
                        @if ($errors->has('first_name_e'))
                        <span class="text-danger invalid-feedback">{{ $errors->first('first_name_e') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'Photo:', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        <input type="file" class="form-control input-radius avatar" name="avatar">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'Email Address:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::text('email', $isEdit ? $admin->email : null, ['class' => 'form-control input-radius email '.($errors->has('email') ? 'is-invalid' : '')]) !!}
                        @if ($errors->has('email'))
                        <span class="text-danger invalid-feedback">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'Password:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::password('password', ['class' => 'form-control input-radius password '.($errors->has('password') ? 'is-invalid' : '')]) !!}
                        @if ($errors->has('password'))
                        <span class="text-danger invalid-feedback">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'Password Confirm:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::password('password_confirmation', ['class' => 'form-control input-radius password_confirmation']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'Gender:', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        <div class="radio">
                            <label class="">
                                <input type="radio" name="gender" class="minimal gender" value="0" {{ ($isEdit && !$admin->gender) ? 'checked' : '' }}>Male
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" class="minimal gender" value="1" {{ ($isEdit && $admin->gender) ? 'checked' : '' }}>Female
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group date">
                    {!! Form::label('title', 'Birthday:<span class="text-red">*</span>', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-4">
                        {!! Form::text('birthday', $isEdit ? ($admin->birthday ? date('Y/m/d', strtotime($admin->birthday)) : '') : null, ['class' => 'form-control input-radius datepicker birthday '.($errors->has('birthday') ? 'is-invalid' : ''), 'placeholder' => 'mm/dd/yyyy']) !!}
                        @if ($errors->has('birthday'))
                        <span class="text-danger invalid-feedback">{{ $errors->first('birthday') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('title', 'Address:', ['class'=> 'col-sm-3 control-label'], false) !!}
                    <div class="col-sm-6">
                        {!! Form::text('address', $isEdit ? $admin->address : null, ['class' => 'form-control input-radius address']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-center">
        <button type="submit" class="btn btn-primary">@lang('admin.common.save')</button>
    </div>
    <!-- /.box-footer -->
{!! Form::close () !!}