@extends('layouts.admin')
@section('title', 'Administrator Detail')
@push('styles')
    <style>
        .btn-edit {
            margin-bottom: 10px;
        }
    </style>
@endpush
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="breadcrumb-title">
            Administrator
            <small>Detail #{{ $admin->id }}</small>
        </h1>
        <div class="pull-right">
            <a href="{{ route('administrators.create') }}" class="btn bg-green btn-flat"><i class="fa fa-plus"></i> New</a>
            <a href="{{ route('administrators.index') }}" class="btn bg-green btn-flat"><i class="fa fa-list"></i> List</a>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#infomation" data-toggle="tab" aria-expanded="true">Infomation</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="infomation">
                            <div class="row">
                                <div class="col-md-8">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th class="text-center">Administrator Name</th>
                                                <td class="text-center">{{ $admin->name }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-center">Email</th>
                                                <td class="text-center">{{ $admin->email }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-center">Gender</th>
                                                <td class="text-center">{{ $admin->gender ? 'Female' : 'Male' }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-center">Address</th>
                                                <td class="text-center">{{ $admin->address }}</td>
                                            </tr>
                                        </tbody>
                                </table>
                                </div>
                                <div class="col-md-4">
                                    <div class="detail-right text-center">
                                        <div class="w100">
                                            <a href="{{ route('administrators.edit', $admin->id) }}" class="btn btn-primary btn-flat btn-edit"><i class="fa fa-fw fa-edit"></i>Edit</a>
                                        </div>
                                        <div class="w100">
                                            @if($admin->avatar)
                                            <img src="{{ Storage::url('administrator/'.$admin->avatar) }}" alt="" class="img-thumbnail">
                                            @else
                                            <img src="{{ asset('assets/avatars/avatar0.jpg') }}" alt="" class="img-thumbnail">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection