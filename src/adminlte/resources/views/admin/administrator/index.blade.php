@extends('layouts.admin')
@section('title', 'Administrator List')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="breadcrumb-title">
            Administrator
            <small>List</small>
        </h1>
        <div class="pull-right">
            <a href="{{ route('administrators.create') }}" class="btn bg-green btn-flat"><i class="fa fa-plus"></i> New</a>
        </div>
    </section>
    <section class="content">
        @include('flash::message')
        <div class="row box-search">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filter Administrator List</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['method' => 'GET', 'url' => route("administrators.index"), 'class' => 'form-horizontal']) !!}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('title', 'Name', ['class'=> 'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                            {!! Form::text('name', request('name'), ['class' => 'form-control input-radius name']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('title', 'Email', ['class'=> 'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                            {!! Form::text('email', request('email'), ['class' => 'form-control input-radius email']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                            <button type="submit" class="btn btn-primary">{{ __('admin.common.search') }}</button>
                        </div>
                    <!-- /.box-footer -->
                    {!! Form::close () !!}
                </div>
            </div>
        </div>
        <div class="row box-result">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Listing</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Birthday</th>
                                    <th>Role</th>
                                    <th class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($administrators as $val)
                                <tr>
                                    <td>{{ $val->id }}</td>
                                    <td>{{ $val->name }}</td>
                                    <td>{{ $val->email }}</td>
                                    <td class="text-center">{{ $val->birthday ? date('Y/m/d', strtotime($val->birthday)) : '' }}</td>
                                    <td>{!! $val->roles->pluck('name')[0] !!}</td>
                                    <td class="text-center">
                                        <!-- <button type="button" class="btn btn-primary btn-sm">Login</button> -->
                                        <a href="{{ route('administrators.show', $val->id) }}" class="btn btn-primary btn-sm">Show</a>
                                        <a href="{{ route('administrators.edit', $val->id) }}" class="btn btn-info btn-sm">Edit</a>
                                        {!! Form::open(['url' => route('administrators.destroy', $val->id),'method' => 'DELETE', 'class' => 'form-delete']) !!}
                                        <button type="button" class="btn btn-danger btn-sm btn-delete" data-id="{{ $val->id }}" data-table="administrators">Delete</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix float-right">
                        {!! $administrators->appends(request()->query())->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection