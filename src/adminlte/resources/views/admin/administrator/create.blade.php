@extends('layouts.admin')
@section('title', 'Administrator Create')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1 class="breadcrumb-title">
            Administrator
            <small>New</small>
        </h1>
        <div class="pull-right">
            <a href="{{ route('administrators.index') }}" class="btn bg-green btn-flat"><i class="fa fa-list"></i> List</a>
        </div>
    </section>
    <section class="content">
        @include('flash::message')
        <div class="row box-search">
            <div class="col-md-12">
                <div class="box">
                    <!-- /.box-header -->
                    <!-- form start -->
                    @includeIf('admin.administrator.form')
                </div>
            </div>
        </div>
    </section>
</div>
@endsection