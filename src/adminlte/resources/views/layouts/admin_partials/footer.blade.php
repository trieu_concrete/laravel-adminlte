<footer class="main-footer">
    <div class="pull-right hidden-xs">
    <b>AdminLTE Portal</b>
    </div>
    <strong>© 2019 <a href="https://ryugaku.merise.asia/">AdminLTE Co.Ltd</a>.</strong> All rights
    reserved.
</footer>