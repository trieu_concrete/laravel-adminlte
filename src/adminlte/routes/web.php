<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', 'Admin\AuthController@login')->name('admin.login');
Route::post('admin/login', 'Admin\AuthController@postLogin')->name('admin.post.login');
Route::get('admin/logout', 'Admin\AuthController@logout')->name('admin.logout');

Route::prefix('admin')
    ->namespace('Admin')
    ->middleware(['auth:admin'])
    ->group(base_path('routes/admin.php'));
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
