<?php
/**
 * Created by VSC.
 * User: trieunb@concrete-corp.com
 * Date: 2019/05/07
 * Time: 
 */

return [
    'admin' => [
        'per_page' =>  env('ADMIN_PER_PAGE', 20),
    ],
    'email' => [
        'admin' => env('ADMIN_EMAIL', 'admin@admin.com'),
    ]
];
